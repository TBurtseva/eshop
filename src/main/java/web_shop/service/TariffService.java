package web_shop.service;

import web_shop.model.Tariff;

import java.util.List;

public interface TariffService {
    public void addTariff(Tariff tariff);

    public void removeTariff(int id);

    public Tariff getTariffById(int id);

    public List<Tariff> getTariffs();
}
