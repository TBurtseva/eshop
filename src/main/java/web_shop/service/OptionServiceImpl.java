package web_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web_shop.dao.OptionDAO;
import web_shop.model.Option;

import java.util.List;

@Service
public class OptionServiceImpl implements OptionService {
    @Autowired
    private OptionDAO optionDAO;

    @Override
    @Transactional
    public void addOption(Option option) {
        optionDAO.addOption(option);

    }

    @Override
    @Transactional
    public void removeOption(int id) {
        optionDAO.removeOption(id);

    }

    @Override
    @Transactional
    public Option getOptionById(int id) {
        return optionDAO.getOptionById(id);
    }

    @Override
    @Transactional
    public List<Option> getOptions() {
        return optionDAO.getOptions();
    }
}
