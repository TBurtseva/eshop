package web_shop.service;

import web_shop.model.Option;

import java.util.List;

public interface OptionService {

    public void addOption(Option option);

    public void removeOption(int id);

    public Option getOptionById(int id);

    public List<Option> getOptions();
}
