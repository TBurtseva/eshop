package web_shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web_shop.dao.TariffDAO;
import web_shop.model.Tariff;

import java.util.List;

@Service
public class TariffServiceImpl implements TariffService {

    @Autowired
    private TariffDAO tariffDAO;

    @Override
    @Transactional
    public void addTariff(Tariff tariff) {
        tariffDAO.addTariff(tariff);
    }

    @Override
    @Transactional
    public void removeTariff(int id) {
        tariffDAO.removeTariff(id);

    }

    @Override
    @Transactional
    public Tariff getTariffById(int id) {
        return tariffDAO.getTariffById(id);
    }

    @Override
    @Transactional
    public List<Tariff> getTariffs() {
        return tariffDAO.getTariffs();
    }
}
