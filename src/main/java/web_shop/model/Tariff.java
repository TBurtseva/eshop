package web_shop.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tariff")
public class Tariff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int tariffId;

    @Column(name ="tariff_name")
    private String tariffName;

    @Column(name = "tariff_price")
    private double tariffPrice;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.DETACH, CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinTable(name = "tariff_option", joinColumns = @JoinColumn(name = "tariff_id"),
               inverseJoinColumns = @JoinColumn(name = "option_id"))
    private List<Option> options;

    public  void addOption(Option option){
        if (options == null){
            options = new ArrayList<>();
        }
        options.add(option);
    }

    public Tariff(){

    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public double getTariffPrice() {
        return tariffPrice;
    }

    public void setTariffPrice(double tariffPrice) {
        this.tariffPrice = tariffPrice;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

//    @Override
//    public String toString() {
//        return "Tariff{" +
//                "tariffId=" + tariffId +
//                ", tariffName='" + tariffName + '\'' +
//                ", tariffPrice=" + tariffPrice +
//                ", options=" + options +
//                '}';
//    }
}
