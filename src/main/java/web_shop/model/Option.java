package web_shop.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "all_options")
public class Option {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int optionId;

    @Column(name = "option_name")
    private String optionName;

    @Column(name = "option_price")
    private double optionPrice;

    @Column(name = "add_price")
    private double addOptionPrice;

//    @ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.DETACH, CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
//    @JoinTable(name = "tariff_option", joinColumns = @JoinColumn(name = "option_id"), inverseJoinColumns = @JoinColumn(name = "tariff_id"))
//    private List<Tariff> tariffs;

    @ManyToMany(mappedBy = "options")
    private List<Tariff> tariffs;

    public Option(){

    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public double getOptionPrice() {
        return optionPrice;
    }

    public void setOptionPrice(double optionPrice) {
        this.optionPrice = optionPrice;
    }

    public double getAddOptionPrice() {
        return addOptionPrice;
    }

    public void setAddOptionPrice(double addOptionPrice) {
        this.addOptionPrice = addOptionPrice;
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<Tariff> tariffs) {
        this.tariffs = tariffs;
    }

    @Override
    public String toString() {
        return "Option{" +
                "optionId=" + optionId +
                ", optionName='" + optionName + '\'' +
                ", optionPrice=" + optionPrice +
                ", addOptionPrice=" + addOptionPrice +
                ", tariffs=" + tariffs +
                '}';
    }
}
