package web_shop.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web_shop.model.Option;

import java.util.List;

@Repository
public class OptionDAOImpl implements OptionDAO {
    private static final Logger logger = LoggerFactory.getLogger(TariffDAOImpl.class);
    // need to inject the session factory
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addOption(Option option) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(option);
        logger.info("Option successfully saved. Option details: " + option);
    }

    @Override
    public void removeOption(int id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Option option = currentSession.load(Option.class, new Integer(id));
        if(option!=null){
            currentSession.delete(option);
        }
        logger.info("Option successfully removed. Option details: " + option);

    }

    @Override
    public Option getOptionById(int id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Option option = currentSession.load(Option.class, new Integer(id));
        logger.info("Option successfully loaded. Option details: " + option);
        return option;
    }

    @Override
    public List<Option> getOptions() {
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Option> theQuery = currentSession.createQuery("from Option order by name", Option.class);
        List<Option> options = theQuery.getResultList();
        return options;
    }
}
