package web_shop.dao;

import web_shop.model.Customer;
import java.util.List;

public interface CustomerDAO {

    public List<Customer> getCustomers();

    void saveCustomer(Customer theCustomer);
}
