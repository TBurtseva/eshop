package web_shop.dao;

import web_shop.model.Tariff;

import java.util.List;

public interface TariffDAO {

    public void addTariff(Tariff tariff);

    public void removeTariff(int id);

    public Tariff getTariffById(int id);

    public List<Tariff> getTariffs();
}
