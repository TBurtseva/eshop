package web_shop.dao;

import web_shop.model.Option;

import java.util.List;

public interface OptionDAO {

    public void addOption(Option option);

    public void removeOption(int id);

    public Option getOptionById(int id);

    public List<Option> getOptions();
}
