package web_shop.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import web_shop.model.Tariff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


@Repository
public class TariffDAOImpl implements TariffDAO {
    private static final Logger logger = LoggerFactory.getLogger(TariffDAOImpl.class);
    // need to inject the session factory
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addTariff(Tariff tariff) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(tariff);
        logger.info("Tariff successfully saved. Tariff details: " + tariff);
    }

    @Override
    public void removeTariff(int id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Tariff tariff = currentSession.load(Tariff.class, new Integer(id));
        if(tariff!=null){
            currentSession.delete(tariff);
        }
        logger.info("Tariff successfully removed. Tariff details: " + tariff);
    }

    @Override
    public Tariff getTariffById(int id) {
        Session currentSession = sessionFactory.getCurrentSession();
        Tariff tariff = currentSession.load(Tariff.class, new Integer(id));
        logger.info("Tariff successfully loaded. Tariff details: " + tariff);
        return tariff;
    }

    @Override
    public List<Tariff> getTariffs() {
        Session currentSession = sessionFactory.getCurrentSession();
        Query<Tariff> theQuery = currentSession.createQuery("from Tariff order by tariff_price", Tariff.class);
        List<Tariff> tariffs = theQuery.getResultList();
        return tariffs;
    }
}
