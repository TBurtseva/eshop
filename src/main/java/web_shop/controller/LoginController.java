package web_shop.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import web_shop.model.Customer;

@Controller
public class LoginController {

	@GetMapping("/showMyLoginPage")
	public String showMyLoginPage() {
        return "login";
	}

	//add request mapping for /access-denied
	@GetMapping("/access-denied")
	public String showAccessDeniedPage() {
		return "access-denied";
	}

    @GetMapping("/admin")
    @Secured("ROLE_ADMIN")
    public String showAdminPage() {
        return "admin";
    }

    @GetMapping("/user")
    @PreAuthorize("hasAuthority('user')")
    public String showUserPage() {
        return "user";
    }

    @GetMapping("/showRegistrationPage")
    public String showRegistrationPage(Model model) {
        Customer theCustomer = new Customer();
        model.addAttribute("customer",theCustomer);
        return "registration";
    }
}
