package web_shop.controller;

import org.springframework.web.bind.annotation.*;
import web_shop.model.Option;
import web_shop.model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import web_shop.service.TariffService;

import java.util.List;


@Controller
@RequestMapping("/tariff")
public class TariffController {

    //need to inject the customer service
    @Autowired
    private TariffService tariffService;

    @GetMapping("/tariffs")
    public String showAllTariffs(Model model) {

        //get tariffs from the service
        List<Tariff> tariffs = tariffService.getTariffs();

        //add the tariffs to the model
        model.addAttribute("tariffs", tariffs);

        return "tariffs";
    }

    @GetMapping("/showTariffAddPage")
    public String showTariffAddPage(Model model){
        //create model attribute to bind form data
        Tariff tariff = new Tariff();
        model.addAttribute("tariff",tariff);
        return "tariff-add";
    }

    @PostMapping("/addTariff")
    public String addTariff(@ModelAttribute("tariff") Tariff tariff){
        //save the tariff using service
        tariffService.addTariff(tariff);
        return "redirect:/tariffs";
    }

    @GetMapping("/showTariffDetails")
    public String showTariffDetails(Model model, @RequestParam("tariff_id") int tariffId) {
        Tariff tariffEntity = tariffService.getTariffById(tariffId);
        List<Option> options = tariffEntity.getOptions();
        model.addAttribute("tariff",tariffEntity);
        model.addAttribute("options",options);
        return "tariff_details";
    }
}