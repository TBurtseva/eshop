<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">
<head>
    <title>Tariffs Catalog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="<c:url value="/resources/css/tariffs.css"/>" rel="stylesheet">
    <link rel="stylesheet" href="https://bestjquery.com/tutorial/css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" href="https://bestjquery.com/tutorial/css/common-1.css"/>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">T-Shop</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/">Home</a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/showRegistrationPage"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
    </div>
</nav>

<div class="demo">
    <div class="container">
        <div class="text-center">
            <h1>Tariffs Catalog</h1>
            <p class="lead">Choose your plan. See what you’ll pay.</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <c:forEach var="tempTariff" items="${tariffs}">
                    <div class="pricingTable-header">
                        <span class="icon"><i class="fa fa-globe"></i></span>
                        <h3 class="title">${tempTariff.tariffName}</h3>
                        <div class="price-value">
                            <span class="currency">$</span>${tempTariff.tariffPrice}
                            <span class="month">/month</span>
                        </div>
                    </div>
                    <div class="pricing-content">
                        <ul>
                            <li>50GB Disk Space</li>
                            <li>50 Email Accounts</li>
                            <li>50GB Monthly Bandwidth</li>
                            <li>10 Subdomains</li>
                            <li>15 Domains</li>
                        </ul>
                        <a onclick="window.location.href='showTariffDetails?tariff_id=${tempTariff.tariffId}'; return false;"
                           class="pricingTable-signup">Get started</a>
                    </div>
                    </c:forEach>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
