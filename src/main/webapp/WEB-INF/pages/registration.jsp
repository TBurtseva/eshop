<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en" >

<head>
    <title>T-Shop/ Registration Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Bootstrap core CSS -->
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>--%>
    <%--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>--%>
    <!-- Custom styles for this template -->
    <link href="<c:url value="/resources/css/registration.css"/>" rel="stylesheet">

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">T-Shop</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/">Home</a></li>
            <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="${pageContext.request.contextPath}/showRegistrationPage"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
    </div>
</nav>

<div class="text-center">
    <main role="main">
        <form:form action="createNewCustomer" modelAttribute="customer" method="POST">
            <img class="mb-4" src="<c:url value="/resources/img/user.png"/>" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Please register</h1>

            <label for="firstrname" class="sr-only">First Name</label>
            <form:input path="firstName" class="form-control" type="text" name="Firstname" id="firstrname" placeholder="First Name" />

            <label for="lastname" class="sr-only">Last Name</label>
            <form:input path="lastName" class="form-control" type="text" name="Lastname" id="lastname" placeholder="Last Name" />

            <%--<label for="birthday" class="sr-only">Birthday</label>--%>
            <%--<form:input path="birthday" class="form-control" type="date" name="Birthday" id="birthday" placeholder="Date Of Birth" />--%>

            <label for="address" class="sr-only">Address</label>
            <form:input path="address" class="form-control" type="text" name="address" id="address" placeholder="Address" />

            <label for="email" class="sr-only">Email</label>
            <form:input path="email" class="form-control" type="email" name="email" id="email" placeholder="Email" />

            <label for="password" class="sr-only">Password</label>
            <form:input path="password" class="form-control" type="password" name="password" id="password" placeholder="Password" />

            <p>

            </p>
            <%--<button class="btn btn--form" type="submit">Register</button>--%>
            <input type="submit" value="Register" class="btn btn--form" />

            <div class="m-t-lg">
                <ul class="list-inline">
                    <li>
                        <a class="signup__link" href="#">I am already a member</a>
                    </li>
                </ul>
            </div>
        </form:form>
    </main>
</div>

</body>

</html>