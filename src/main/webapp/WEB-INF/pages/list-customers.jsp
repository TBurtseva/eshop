<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
    <title>List Customers</title>

    <!-- reference style sheet -->
    <link type="text/css"
          rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

<div id="wrapper">
    <div id="header">
        <h2>List of existing Customers</h2>
    </div>
</div>

<div id="container">

    <div id="content">

        <!-- put button: Add Customer -->
        <input type="button" value="Add Customer"
               onclick="window.location.href='showFormForAdd'; return false;"
               class="add-button"/>

        <!--  add html table here -->
        <table>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Address</th>
                <%--<th>Birthday</th>--%>
            </tr>

            <!-- loop over and print customers -->
            <c:forEach var="tempCustomer" items="${customers}">

                <tr>
                    <td> ${tempCustomer.firstName} </td>
                    <td> ${tempCustomer.lastName} </td>
                    <td> ${tempCustomer.email} </td>
                    <td>${tempCustomer.password}</td>
                    <td>${tempCustomer.address}</td>
                    <%--<td>${tempCustomer.birthday}</td>--%>
                </tr>

            </c:forEach>

        </table>

    </div>

</div>


</body>

</html>