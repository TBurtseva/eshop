<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en" >

<head>
    <title>T-Shop/Login Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="<c:url value="/resources/css/login.css"/>" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">T-Shop</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/">Home</a></li>
            <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/showRegistrationPage"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li class="active"><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
    </div>
</nav>

<div class="text-center">
<main role="main">
        <form:form class="form-signin" action="${pageContext.request.contextPath}/authenticateTheUser" method="POST">
            <div>
                <!-- Check for login error -->
                <c:if test="${param.error != null}">
                    <div class="alert alert-danger form-control">
                        Invalid username and password.
                    </div>
                </c:if>

                <!-- Check for logout -->
                <c:if test="${param.logout != null}">
                    <div class="alert alert-success form-control">
                        You have been logged out.
                    </div>
                </c:if>
            </div>

            <img class="mb-4" src="<c:url value="/resources/img/lock.png"/>" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus/>

            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required/>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" name="remember-me" value="remember-me"> Remember me
                </label>
            </div>
            <input class="btn btn--form" type="submit" value="Login" />
            <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
        </form:form>
</main>
</div>
</body>
</html>