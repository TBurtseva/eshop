<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
    <title>Tariff Details</title>

    <!-- reference style sheet -->
    <link type="text/css"
          rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

<div id="wrapper">
    <div id="header">
        <h2>Tariff Details</h2>
    </div>
</div>

<div id="container">

    <div id="content">

        <!-- put button: Add Customer -->
        <%--<input type="button" value="Add Customer"--%>
               <%--onclick="window.location.href='showFormForAdd'; return false;"--%>
               <%--class="add-button"/>--%>

        <!--  add html table here -->
        <table>
            <tr>
                <th>Name</th>
                <th>Monthly price</th>
                <th>Adding price</th>
            </tr>

            <!-- loop over and print customers -->
            <c:forEach var="tempOption" items="${options}">

                <tr>
                    <td> ${tempOption.optionName} </td>
                    <td> ${tempOption.optionPrice} </td>
                    <td> ${tempOption.addOptionPrice} </td>
                    <td> <!-- Actions for the individual item -->
                        <%--href="/showTariffDetails?all_options_id=${tempOption.optionId}--%>
                        <input type="button" value="Add Option" class="add-button" />
                    </td>
                </tr>

            </c:forEach>

        </table>

    </div>

</div>


</body>

</html>
