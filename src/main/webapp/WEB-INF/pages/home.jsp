<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>T-Shop/Home Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <style>
        body {
            background-image: url(<c:url value="/resources/img/home.jpg"/>);
        }
        .bg-1 h1 {
            color: #fff !important;
        }

    </style>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">T-Shop</a>
        </div>
        <sec:authorize access="isAnonymous()">
        <ul class="nav navbar-nav">
            <li class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
            <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="${pageContext.request.contextPath}/showRegistrationPage"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
        </sec:authorize>

        <sec:authorize access="hasRole('ROLE_USER')">
            <ul class="nav navbar-nav">
                <li class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
                <li><a href="${pageContext.request.contextPath}/contracts">Contracts</a></li>
                <li><a href="${pageContext.request.contextPath}/details">Details</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <form:form action="${pageContext.request.contextPath}/logout" method="post" class="glyphicon glyphicon-log-out">
                    <input type="submit" class="glyphicon glyphicon-log-out" value="Logout" />
                </form:form>
                <%--<li><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>--%>
            </ul>
        </sec:authorize>

        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <ul class="nav navbar-nav">
                <li class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Catalog</a></li>
                <li><a href="${pageContext.request.contextPath}/customer/list">Customers</a></li>
                <li><a href="${pageContext.request.contextPath}/contracts">Contracts</a></li>
                <li><a href="${pageContext.request.contextPath}/tariff/tariffs">Tariffs</a></li>
                <li><a href="${pageContext.request.contextPath}/options">Options</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <form:form action="${pageContext.request.contextPath}/logout" method="post" class="glyphicon glyphicon-log-out">
                    <input type="submit" class="glyphicon glyphicon-log-out" value="Logout" />
                </form:form>
                    <%--<li><a href="${pageContext.request.contextPath}/showMyLoginPage"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>--%>
            </ul>
        </sec:authorize>
    </>
</nav>

<div class="bg-1 text-center">
    <h1>YOUR-Mobile BLACK</h1>
    <p>With our plan, you just get more.</p>
</div>

</body>
</html>